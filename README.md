﻿# Rule Inducer

Rule Inducer (rule-inducer) is a part of Meta-Alert Prioritisation (MAP) module. It is a REST service providing endpoints allowing to induce, and test decision rule models (MCDA models). These models may be used by Ranking Generator (and Criteria Mapper) to prioritise meta-alerts. Other endpoints provided by Rule Inducer allow to store and retrieve meta-data, data,  decision rules, as well as to manage models, and users. More precisely, the following REST endpoints are provided:

Name | Method | Description
---|---|---
`/put-metadata` | `{PUT, POST}` | stores meta-data provided as request body in JSON format, and returns `id` of the stored meta-data in JSON format; optional request parameters are `name` and `description`
`/get-metadata` | `GET` | provides, in JSON format, meta-data identified by request parameter `id` or `user_id` and/or `model_id`; when `model_id` is provided, the meta-data from this model is selected; when only `user_id` is provided, the meta-data from the active model is selected
`/validate-metadata` | * | validates meta-data identified by request parameter `id` or `user_id` and/or `model_id`; when `model_id` is provided, the meta-data from this model is selected; when only `user_id` is provided, the meta-data from the active model is selected; the result of validation is returned in JSON format
`/put-dataset` | `{PUT, POST}` | stores data set provided as request body in JSON format, links data set with meta-data (`metadata_id` or `user_id` and/or `model_id` should be provided as request parameters) and returns `id` of the stored data set in JSON format; when `model_id` is provided, the meta-data from this model is selected; when only `user_id` is provided, the meta-data from the active model is selected; optional request parameters include also `name` and `description` of stored data set
`/get-dataset` | `GET` | provides, in JSON format, data set identified by request parameter `id` or `user_id` and/or `model_id`; when `model_id` is provided, the data set from this model is selected; when only `user_id` is provided, the data set from the active model is selected
`/validate-dataset` | * | validates data set identified by request parameter `id` or `user_id` and/or `model_id`; when `model_id` is provided, the data set from this model is selected; when only `user_id` is provided, the data set from the active model is selected; the result of validation is returned in JSON format
`/learn` | * | induces rules on the basis of a data set and stores them; it returns, in JSON format, `id` of stored rules; `dataset_id` identifying the data set on which rules are induced and, optionally, `metadata_id` should be provided as request parameters; instead of these parameters, `user_id` and/or `model_id` may be provided as request parameters; when `model_id` is provided, the meta-data and the data set from this model are selected; when only `user_id` is provided, the meta-data and the data set from the active model are selected; optional request parameters include also: `rule_type` (`certain` or `possible`), `consistency_threshold` (in range [0, 1]; specified only for certain rules), as well as `name` and `description` of induced (and stored) rules
`/put-rules` | `{PUT, POST}` | stores rules provided as as request body in RuleML format, links rules with meta-data, and (optionally) with data set; `metadata_id` or `dataset_id` or `user_id` and/or `model_id` should be provided as request parameters; when `model_id` is provided, the meta-data and the data set from this model are selected; when only `user_id` is provided, the meta-data and the data set from the active model are selected; it returns, in JSON format, `id` of stored rules; optional request parameters include also `name` and `description` of stored rules
`/get-rules` | `GET` | provides, in RuleML format, rules identified by request parameter `id` or `dataset_id` or `user_id` and/or `model_id`; when `model_id` is provided, rules from this model are selected; when only `user_id` is provided, rules from the active model are selected
`/validate-rules` | * | validates rules identified by request parameter `id` or `dataset_id` or `user_id` and/or `model_id`; when `model_id` is provided, rules from this model are selected; when only `user_id` is provided, rules from the active model are selected; the result of validation is returned in JSON format
`/add-user`  | * | adds user with specified request parameters `id` and `name`; returns `id` of added user in JSON format
`/set-rules-model` | * | constructs (sets) a model by linking rules (`rules_id` should be provided as request parameter) with user (`user_id` should also be provided as request parameter) and returns, in JSON format, `id` of the constructed model; optional request parameters are `name` and `description`
`/set-active-model` | * | sets active model for a user (`user_id` and `model_id` should be provided as request parameters); the `id` of active model is returned in JSON format
`/validate` | * | validates prioritisation of meta-alerts with induced rules; data sets used for validation are obtained by splitting a data set into two disjoint subsets: a validation set (with meta-alerts used for validation) and a learning set (with meta-alerts used to induce rules); the result of validation is returned in JSON format; `dataset_id` identifying the data set used to obtain validation and learning subsets and, optionally, `metadata_id` should be provided as request parameters; instead of these parameters, `user_id` and/or `model_id` may be provided as request parameters; when `model_id` is provided, the meta-data and the data set from this model are selected; when only `user_id` is provided, the meta-data and the data set from the active model are selected; `validation_proportion`, and `learning_proportion`, both in range [0, 1], should be provided to specify proportions in which the data set is splitted into subsets (the sum of both proportions has to be not greater than 1, but can be lower than 1, in which case part of the data set is not used); optional request parameters include also: `rule_type` (`certain` or `possible`), `consistency_threshold` (in range [0, 1]; specified only for certain rules), as well as `seed` used to initialize random number generator
`/test` | * | tests prioritisation of meta-alerts from data set identified by request parameter `dataset_id` with rules model identified by request parameter `rules_id` or `user_id` and/or `model_id`; when `model_id` is provided, rules from this model are selected; when only `user_id` is provided, rules from the active model are selected; the result of test is returned in JSON format
`/init` | * | adds user named `protective`, default meta-data, default set of rules (provided that they were not added yet); creates model using these rules and sets it as active model of the user

# How to install?
## Cloning repository
git clone git@gitlab.com:protective-h2020-eu/meta-alert-prioritisation/rule-inducer.git

## Building

### Managing docker images

To create and push new docker image to the repository, **gradle.properties** file needs to be filled. 
It contains registry credentials such as login, password and repository url. There are **no changes in build.gradle** needed.
After that, proper gradle task can be run (in project dir). 

**Warning:** tag of the docker image will be generated based on *annotated* git tags. 
More info: [semver-git](https://github.com/cinnober/semver-git)
### Gradle tasks
To create only Dockerfile in /build/docker, run:
```
gradlew -q createDockerfile
```


To create Dockerfile and local docker image, run:
```
gradlew -q buildImage
```

To create and push local docker image, run:
```
gradlew -q pushImage
```
**Warning:** It may take some time and create some warnings.


All above tasks can be found in *docker* task group.

# Running
## Examples of usage

In all of examples provided in this document it is assumed that rule-inducer may be accessed on a machine called `host` on port `8009`. 

When starting on an empty database, initialisation of module is taking place. The initialisation is equivalent to running `/init` endpoint, and it consists in: adding default user (with `user_id = protective`), and setting active model (with default set of rules).

### Meta-data
Input meta-data should be specified in JSON, e.g. like in [meta-data used in prioritisation of meta-alerts](docs/model/json/metadata-prioritisation.json), or [example meta-data](docs/model/json/metadata-example.json) listed below:

```json
[{
  "name": "ID",
  "active": true,
  "identifierType": "uuid"
}, {
  "name": "SourceAssetCriticality",
  "active": true,
  "type": "condition",
  "valueType": "enumeration",
  "domain": ["NA", "low", "med", "high", "critical"],
  "preferenceType": "gain"
}, {
  "name": "TargetAssetCriticality",
  "active": true,
  "type": "condition",
  "valueType": "enumeration",
  "domain": ["NA", "low", "med", "high", "critical"],
  "preferenceType": "gain"
}, {
  "name": "TimeFromDetectTime",
  "active": true,
  "type": "condition",
  "valueType": "real",
  "preferenceType": "cost"
}, {
  "name": "Priority",
  "active": true,
  "type": "decision",
  "valueType": "enumeration",
  "domain": ["1", "2", "3", "4", "5"],
  "preferenceType": "cost"
}]
```
The following query command may be used to store meta-data in the database. In this query request parameters: `name` and `description`, may be used to set name and description of stored meta-data.

```
curl -X PUT -H "Content-Type: application/json" -i 'http://host:8009/put-metadata?name=metadata-example&description=exemplary%20meta-data%20describing%20meta-alerts' 
--data '[{
  "name": "ID",
  "active": true,
  "identifierType": "uuid"
}, {
  "name": "SourceAssetCriticality",
  "active": true,
  "type": "condition",
  "valueType": "enumeration",
  "domain": ["NA", "low", "med", "high", "critical"],
  "preferenceType": "gain"
}, {
  "name": "TargetAssetCriticality",
  "active": true,
  "type": "condition",
  "valueType": "enumeration",
  "domain": ["NA", "low", "med", "high", "critical"],
  "preferenceType": "gain"
}, {
  "name": "TimeFromDetectTime",
  "active": true,
  "type": "condition",
  "valueType": "real",
  "preferenceType": "cost"
}, {
  "name": "Priority",
  "active": true,
  "type": "decision",
  "valueType": "enumeration",
  "domain": ["1", "2", "3", "4", "5"],
  "preferenceType": "cost"
}]'
```
The response, consisting of id of stored meta-data, may look like:

```json
{
  "id": 3
}
```

The following query command should be used to retrieve the meta-data that were just stored in the database (meta-data identified by `id = 3`):

```
curl -X GET -i 'http://host:8009/get-metadata?id=3'
```

Finally, to check whether meta-data, stored with `id = 3`, may be correctly parsed the following query command should be used:

```
curl -X GET -i 'http://host:8009/validate-metadata?id=3'
```

The response for meta-data used in this example should look like:

```json
{
  "numberOfAttributes": 5
}
```

This response shows how many attributes have been correctly parsed.

### Data

Input data should be specified in JSON (just like meta-data), like in [example data file](docs/model/json/data-example.json):

```json
[{
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f",
  "SourceAssetCriticality": "low",
  "TargetAssetCriticality": "critical",
  "TimeFromDetectTime": "187296.193",
  "Priority": "2"
}, {
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e",
  "SourceAssetCriticality": "NA",
  "TargetAssetCriticality": "critical",
  "TimeFromDetectTime": "1.0728096193E7",
  "Priority": "3"
}]
```
The following query command may be used to store data in the database. In this query, `metadata_id` of meta-data describing the data should be provided as request parameter (in our case it is `metadata_id = 3`). It allows to link meta-data with the data. Other request parameters include `name` and `description`.

```
curl -X PUT -H "Content-Type: application/json" -i 'http://host:8009/put-dataset?metadata_id=3&name=data-example&description=exemplary%20set%20of%20meta-alerts%20in%20JSON' 
--data '[{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "low",
  "TimeFromDetectTime": 187296.193,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f",
  "Priority": "2"
}, {
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "NA",
  "TimeFromDetectTime": 10728096.193,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e",
  "Priority": "3"
}]'
```
Alternatively, user id may be used to this end (to link meta-data from user's active model with data; see section **Users**, and **Models** for more details). 

The response, consisting of id of stored data, may look like:

```json
{
  "id": 2
}
```
The following query command should be used to retrieve the data that were just stored in the database:

```
curl -X GET -i 'http://host:8009/get-dataset?id=2'
```

Finally, to check whether data may be correctly parsed, the following query command should be used:

```
curl -X GET -i 'http://host:8009/validate-dataset?id=2'
```

The response for data used in this example should look like:

```json
{
  "numberOfAttributes": 5,
  "numberOfObjects": 2,
  "activeDecisionAttribute": true
}
```
This response indicates that two objects described by five attributes, with one active decision attribute, have been parsed.

### Rules

Rules are expressed in a variant of [RuleML](http://ruleml.org) format. An [exemplary set of rules](docs/model/ruleml/rules-example.xml) may take the following form:

```
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://deliberation.ruleml.org/1.01/relaxng/datalogplus_min_relaxed.rnc"?>
<RuleML xmlns="http://ruleml.org/spec">
<act index="1">
<assert>
	<implies>
		<if>
			<atom>
				<op>
					<rel>ge</rel>
				</op>
				<ind>critical</ind>
				<var>SourceAssetCriticality</var>
			</atom>
		</if>
		<then>
			<atom>
				<op>
					<rel>le</rel>
				</op>
				<ind>1</ind>
				<var>Priority</var>
			</atom>
		</then>
	</implies>
</assert>
<assert>
	<implies>
		<if>
			<and>
				<atom>
					<op>
						<rel>le</rel>
					</op>
					<ind>NA</ind>
					<var>SourceAssetCriticality</var>
				</atom>
				<atom>
					<op>
						<rel>le</rel>
					</op>
					<ind>high</ind>
					<var>TargetAssetCriticality</var>
				</atom>
			</and>
		</if>
		<then>
			<atom>
				<op>
					<rel>ge</rel>
				</op>
				<ind>5</ind>
				<var>Priority</var>
			</atom>
		</then>
	</implies>
</assert>
</act>
</RuleML>
```

Other examples of larger sets of rules are: [subset A of rules used to prioritise meta-alerts](docs/model/ruleml/rules-prioritisation-a.xml), [subset B of rules used to prioritise meta-alerts](docs/model/ruleml/rules-prioritisation-b.xml), [full set of rules used to prioritise meta-alerts](docs/model/ruleml/rules-prioritisation.xml).

The following query command may be used to store set of rules in the database. In this query, `metadata_id` of meta-data describing attributes used in rules should be provided (in our case it is `metadata_id = 3`). It allows to link the meta-data with the set of rules. Other request parameters include `name` and `description`.

```
curl -X PUT -i 'http://host:8009/put-rules?metadata_id=3&name=rules-example&description=exemplary%20set%20of%20rules%20in%20RuleML' 
--data '<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://deliberation.ruleml.org/1.01/relaxng/datalogplus_min_relaxed.rnc"?>
<RuleML xmlns="http://ruleml.org/spec">
<act index="1">
<assert>
	<implies>
		<if>
			<atom>
				<op>
					<rel>ge</rel>
				</op>
				<ind>critical</ind>
				<var>SourceAssetCriticality</var>
			</atom>
		</if>
		<then>
			<atom>
				<op>
					<rel>le</rel>
				</op>
				<ind>1</ind>
				<var>Priority</var>
			</atom>
		</then>
	</implies>
</assert>
<assert>
	<implies>
		<if>
			<and>
				<atom>
					<op>
						<rel>le</rel>
					</op>
					<ind>NA</ind>
					<var>SourceAssetCriticality</var>
				</atom>
				<atom>
					<op>
						<rel>le</rel>
					</op>
					<ind>high</ind>
					<var>TargetAssetCriticality</var>
				</atom>
			</and>
		</if>
		<then>
			<atom>
				<op>
					<rel>ge</rel>
				</op>
				<ind>5</ind>
				<var>Priority</var>
			</atom>
		</then>
	</implies>
</assert>
</act>
</RuleML>
```

Alternatively, user id may be used to this end (to link meta-data from user's active model with rules; see section **Users**, and **Models** for more details).

The response, consisting of id of stored rules, may look like:

```json
{
  "id": 2
}
```

The following query command should be used to retrieve the rules that were stored in the database:

```
curl -X GET -i 'http://host:8009/get-rules?id=2'
```

Finally, to check whether rules may be correctly parsed the following query command should be used:

```
curl -X GET -i 'http://host:8009/validate-rules?id=2'
```

The response for the set of rules used in this example should look like:

```json
{
  "numberOfRules": 2
}
```

This response indicates that two rules have been correctly parsed.

On the basis of a data set stored in the database, one may induce decision rules using VC-DomLEM algorithm (J. Błaszczyński, R. Słowiński, and M. Szeląg, “Sequential covering rule induction algorithm for variable consistency rough set approaches,” Information Sciences, vol. 181, no. 5, pp. 987–1002, 2011). The following query command may be used to induce rules on the basis of a data set identified by `dataset_id = 3`:

```
curl -X GET -i 'http://host:8009/learn?dataset_id=3&name=induced-rules-example&description=exemplary%20set%20of%20induced%20rules'
```

The response, consisting of id of induced (and stored) rules, may look like:

```json
{
  "id": 3
}
```

While inducing decision rules, one may also specify the type of induced rules by setting request parameter `rules_type`. Two types of rules may be induced by VC-DomLEM: certain rules and possible rules. When request parameter `rules_type` is set to any other value than `possible` certain rules are induced. Only for certain rules `consistency_threshold` request parameter may be additionally specified. This parameter takes values from range [0,1] and indicates acceptable level of inconsistency of induced rules (i.e., consistency threshold is of cost type: the higher value the more inconsistent rules may be induced). 


### Users

Users may be added by using endpoint `/add-user` with two required request parameters: `id` and `name`. The following query may be used to add a new user with `id = jurek`:

```
curl -X PUT -i 'http://host:8009/add-user?id=jurek&name=Jurek%20Blaszczynski'
```

The response for the above query should look like:

```json
{
  "id": "jurek"
}
```

### Models

Models link rules with users. To create a new model (e.g., called `example-model`), linking existing user (e.g., `jurek`) and rules stored in the database (e.g., identified by `rules_id = 2`), the following query may be used:

```
curl -X PUT 
-i 'http://host:8009/set-rules-model?user_id=jurek&rules_id=2&name=example-model&description=Exemplary%20rules%20model'
```

The response, consisting of id of created model, should look like:

```json
{
  "id": 2
}
```

Each user may have set one special model called **active model**. This model is used by default in all queries issued by the user. To set previously stored model with `model_id = 2` as active model of user `jurek`, the following query may be used:

```
curl -X PUT -i 'http://host:8009/set-active-model?user_id=jurek&model_id=2'
```

The response, consisting of id of active model, should look like:

```json
{
  "id": 2
}
```
