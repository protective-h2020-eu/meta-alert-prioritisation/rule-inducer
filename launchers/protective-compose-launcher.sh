#!/usr/bin/env sh

# pass Java 1.8 JDK path (absolute) as the first argument

# Go to main project directory
cd ..

# Clean first
docker stop ri
docker rm ri
docker stop postgresdb
docker rm postgresdb

export JAVA_HOME=$1
export PATH=$JAVA_HOME\bin:$PATH

./gradlew clean build createDockerfile -x test

# Save some disk space...
#del build\libs\rule-inducer*.war

docker-compose up --build

# Save some disk space...
#del build\docker\rule-inducer*.war
