@ECHO OFF

REM pass Java 1.8 JDK path (absolute) as the first argument

IF "%~1" == "" GOTO :usage

REM Go to main project directory
CD ..

REM Clean first
call docker stop ri
call docker rm ri
call docker stop postgresdb
call docker rm postgresdb

call docker network rm protective-net

SET JAVA_HOME=%1%
SET PATH=%JAVA_HOME%\bin;%PATH%

call gradlew clean build createDockerfile -x test
call docker build -t rule-inducer build/docker

REM save some disk space...
del build\docker\rule-inducer*.war
del build\libs\rule-inducer*.war

REM Create common network for both containers
call docker network create protective-net

REM Start postgres DB
call docker run --name postgresdb --rm -p 5432:5432 -e POSTGRES_DB=protective -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -d --net=protective-net postgres

REM Start rule-inducer with env
call docker run --name ri --rm -it -p 8009:8009 -e WAIT_FOR_HOST_AND_PORT=postgresdb:5432 -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgresdb:5432/protective -e SPRING_DATASOURCE_USERNAME=postgres -e SPRING_DATASOURCE_PASSWORD=mysecretpassword --net=protective-net rule-inducer:latest

pause
:usage
echo Wrong number of parameters. Exactly one parameter required.
echo ------
echo Usage:
echo ------
echo protective-simple-launcher "<java-1.8-jdk-path>"