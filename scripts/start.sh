#!/bin/sh

echo "Configuring rule-inducer..."

# Wait for DB to be ready:
if [ -n "$WAIT_FOR_HOST_AND_PORT" ]; then
	HOST_AND_PORT=$WAIT_FOR_HOST_AND_PORT
	echo "Started waiting for $HOST_AND_PORT"

	/app/wait-for-it.sh $HOST_AND_PORT -- java -jar /app/rule-inducer.war
fi

# echo "Starting rule-inducer..."
# java -jar /app/rule-inducer.war