package eu.h2020.protective.meta_alert_prioritisation.rule_inducer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RuleInducerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
