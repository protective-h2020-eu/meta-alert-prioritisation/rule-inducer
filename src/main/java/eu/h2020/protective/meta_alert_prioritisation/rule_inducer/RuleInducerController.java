/**
 * Copyright (C) Jerzy Błaszczyński, Marcin Szeląg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.h2020.protective.meta_alert_prioritisation.rule_inducer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.rulelearn.approximations.ClassicalDominanceBasedRoughSetCalculator;
import org.rulelearn.approximations.Union;
import org.rulelearn.approximations.Unions;
import org.rulelearn.approximations.UnionsWithSingleLimitingDecision;
import org.rulelearn.approximations.VCDominanceBasedRoughSetCalculator;
import org.rulelearn.classification.ClassificationResult;
import org.rulelearn.classification.SimpleClassificationResult;
import org.rulelearn.classification.SimpleRuleClassifier;
import org.rulelearn.core.InvalidSizeException;
import org.rulelearn.core.InvalidValueException;
import org.rulelearn.data.Attribute;
import org.rulelearn.data.AttributeType;
import org.rulelearn.data.Decision;
import org.rulelearn.data.EvaluationAttribute;
import org.rulelearn.data.InformationTable;
import org.rulelearn.data.InformationTableWithDecisionDistributions;
import org.rulelearn.data.SimpleDecision;
import org.rulelearn.data.json.AttributeParser;
import org.rulelearn.data.json.ObjectParser;
import org.rulelearn.measures.ConsistencyMeasure;
import org.rulelearn.measures.dominance.EpsilonConsistencyMeasure;
import org.rulelearn.rules.RuleSet;
import org.rulelearn.rules.ruleml.RuleMLBuilder;
import org.rulelearn.rules.ruleml.RuleParser;
import org.rulelearn.sampling.Splitter;
import org.rulelearn.types.ElementList;
import org.rulelearn.types.EnumerationField;
import org.rulelearn.types.EnumerationFieldFactory;
import org.rulelearn.validation.ClassificationValidationResult;
import org.rulelearn.validation.OrdinalMisclassificationMatrix;
import org.rulelearn.wrappers.PossibleVCDomLEMWrapper;
import org.rulelearn.wrappers.VCDomLEMWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.server.ResponseStatusException;

import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.AttributesDescription;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.ClassificationTestResult;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.Datasets;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.DatasetsRepository;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.InformationTableDescription;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.Metadata;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.MetadataRepository;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.Models;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.ModelsRepository;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.Rules;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.RulesDescription;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.RulesRepository;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.TruePositiveRateDescription;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.UnionDescription;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.Users;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.UsersRepository;
import eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model.json.JSONString;

/**
 * Spring Boot REST controller wrapping ruleLearn rule induction, as well as: data sets, meta-data, model, and users management methods.
 *
 * @author Jerzy Błaszczyński (<a href="mailto:jurek.blaszczynski@cs.put.poznan.pl">jurek.blaszczynski@cs.put.poznan.pl</a>)
 * @author Marcin Szeląg (<a href="mailto:marcin.szelag@cs.put.poznan.pl">marcin.szelag@cs.put.poznan.pl</a>)
 */
@RestController
public class RuleInducerController {
	
	/**
	 * Default user ID.
	 */
	public static final String DEFAULT_USER_ID = "protective";
	
	/**
	 * Default rule set ID.
	 */
	public static final int DEFAULT_RULE_SET_ID = 1;
	
	/**
	 * Default (empty) rule set represented as RuleML string.
	 */
	public static final String DEFAULT_RULES_RULEML_STRING = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<?xml-model href=\"http://deliberation.ruleml.org/1.01/relaxng/datalogplus_min_relaxed.rnc\"?>\n" + 
			"<RuleML xmlns=\"http://ruleml.org/spec\">\n" + 
			"</RuleML>";
	
	/**
	 * Application context used to access resources.
	 */
	private ApplicationContext appContext;
	
	/**
	 * Application environment.
	 */
	@Autowired
	private Environment environment;
	
	/**
	 * Logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(RuleInducerController.class);
	
	/**
	 * Repository with users.
	 */
	@Autowired
	private UsersRepository usersRepository;
	
	/**
	 * Repository with meta-data
	 */
	@Autowired
	private MetadataRepository metaDataRepository;
	
	/**
	 * Repository with data sets.
	 */
	@Autowired
	private DatasetsRepository dataSetsRepository;
	
	/**
	 * Repository with rules.
	 */
	@Autowired
	private RulesRepository rulesRepository;
		
	/**
	 * Repository with models.
	 */
	@Autowired
	private ModelsRepository modelsRepository;
	
	/**
	 * Constructs this controller and sets application context.
	 */
	public RuleInducerController () {
		super();
		this.appContext = new GenericApplicationContext();
	}
	
	/**
	 * Provides a model {@link Models} for given ID of a user and/or ID of a model. 
	 * 
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); model of the user provided
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		 active model of the user provided
	 * @return model {@link Models} or {@code null} when it is impossible to obtain one 
	 */
	private Models getModel (String userId, Long modelId) {
		Users user = null;
		Models model = null;
		
		if (userId != null) {
			if (usersRepository.findById(userId).isPresent()) {
				user = usersRepository.findById(userId).get();
				if (modelId != null) {
					if (modelsRepository.findById(modelId).isPresent()) {
						model = modelsRepository.findById(modelId).get();
					}
				}
				else if (user.getActiveModel() != null) {
					model = user.getActiveModel();
				}
			}
		}
		else {
			if (modelId != null) {
				if (modelsRepository.findById(modelId).isPresent()) {
					model = modelsRepository.findById(modelId).get();
				}
			}
		}
		
		return model;
	}
	
	/**
	 * Provides meta-data {@link Metadata} for given ID of metadata and/or ID of a user and/or ID of a model. 
	 * 
	 * @param metadataId ID of meta-data in database (when this ID is {@code null} user ID and/or model ID is taken) that is provied
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); data set from active model of the user is provided
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		 meta-data from the model is provided
	 * @return meta-data {@link Metadata} or {@code null} when it is impossible to obtain it
	 */
	private Metadata getMetadataByIdOrModel(Long metadataId, String userId, Long modelId) {
		Metadata metadata = null;
		
		if (metadataId != null) {
			if (metaDataRepository.findById(metadataId).isPresent()) {
				metadata = metaDataRepository.findById(metadataId).get();
			}
		}
		else {
			Models model = getModel(userId, modelId);
			if (model != null) {
				metadata = model.getRules().getMetadata();
			}
		}
		
		return metadata;
	}
	
	/**
	 * Provides a data set {@link Datasets} for given ID of data set and/or ID of a user and/or ID of a model. 
	 * 
	 * @param dataSetId ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) that is provided
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); data set from active model of the user is provided
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		data set from the model is provided
	 * @return data set {@link Datasets} or {@code null} when it is impossible to obtain it
	 */
	private Datasets getDataSetByIdOrModel(Long dataSetId, String userId, Long modelId) {
		Datasets dataSet = null;
		
		if (dataSetId != null) {
			if (dataSetsRepository.findById(dataSetId).isPresent()) {
				dataSet = dataSetsRepository.findById(dataSetId).get();
			}
		}
		else {
			Models model = getModel(userId, modelId);
			if (model != null) {
				dataSet = model.getRules().getDataset();
			}
		}
		
		return dataSet;
	}
	
	/**
	 * Provides rules {@link Rules} for given ID of rules and/or ID of a user and/or ID of a model. 
	 * 
	 * @param rulesId ID of rules in database (when this ID is {@code null} user ID and/or model ID is taken) that is provided
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); rules from active model of the user are provided
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		rules from the model are provided
	 * @return rules {@link Rules} or {@code null} when it is impossible to obtain it
	 */
	private Rules getRulesByIdOrModel(Long rulesId, String userId, Long modelId) {
		Rules rules = null;
		
		if (rulesId != null) {
			if (rulesRepository.findById(rulesId).isPresent()) {
				rules = rulesRepository.findById(rulesId).get();
			}
		}
		else {
			Models model = getModel(userId, modelId);
			if (model != null) {
				rules = model.getRules();
			}
		}
		
		return rules;
	}
	
	/**
	 * Provides an array of attributes {@link Attribute} on the basis of the provided meta-data {@link Metadata}.
	 * 
	 * @param metadata attributes to be provided
	 * @return an array of attributes {@link Attribute}
	 */
	private Attribute[] getAttributes(Metadata metadata) {
		Attribute[] attributes = null;
		
		if (metadata != null) {
			AttributeParser attributeParser = new AttributeParser();
			try (StringReader attributeReader = new StringReader(metadata.getData())) {
				attributes = attributeParser.parseAttributes(attributeReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
				throw new ResponseStatusException(
				          HttpStatus.BAD_REQUEST, "Error while parsing meta-data: " + ex.toString());
			}
		}
		
		return attributes;
	}
	
	/**
	 * Provides an information table (table with objects) {@link InformationTable} on the basis of the provided data set {@link Datasets}.
	 * 
	 * @param dataset objects to be provided
	 * @return an array of attributes {@link Attribute}
	 */
	private InformationTable getObjects(Datasets dataset) {
		InformationTable informationTable = null;
		
		Attribute[] attributes = getAttributes(dataset.getMetadata());
		if (attributes != null) {
			ObjectParser objectParser = new ObjectParser.Builder(attributes).build();
			try (StringReader objectReader = new StringReader(dataset.getData())) {
				informationTable = objectParser.parseObjects(objectReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
				throw new ResponseStatusException(
				          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples: " + ex.toString());
			}
		}
				
		return informationTable;
	}
	
	/**
	 * Provides set of rules {@link RuleSet} on the basis of the provided rules {@link Rules}.
	 * 
	 * @param rules rules to be provided
	 * @return set of rules {@link RuleSet}
	 */
	private RuleSet getRuleSet(Rules rules) {
		RuleSet ruleSet = null;
		
		if (rules != null) {
			Attribute [] attributes = getAttributes(rules.getMetadata());  
			if (attributes != null) {
				Map<Integer, RuleSet> allRules = null;
				RuleParser ruleParser = new RuleParser(attributes);
				try (InputStream rulesStream = IOUtils.toInputStream(rules.getData(), StandardCharsets.UTF_8.name())) {
					allRules = ruleParser.parseRules(rulesStream);
				}
				catch (IOException ex) {
					logger.error(ex.toString());
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing rules: " + ex.toString());
				}
				if ((allRules != null) && (allRules.size() > 0)) {
					ruleSet = allRules.get(DEFAULT_RULE_SET_ID);
				}
			}			
		}
		
		return ruleSet;
	}
	
	/**
	 * Induces certain decision rules satisfying consistency threshold (in case when it is not {@code null}) on the basis of the provided data set.
	 * 
	 * @param dataset data set {@link Datasets} on which rules are induced
	 * @param consistencyThreshold threshold on a consistency measure {@link ConsistencyMeasure}, if it is {@code null} then 
	 * 			consistency threshold is not taken into account 
	 * 
	 * @return certain decision rules in ruleML format
	 */
	private String induceCertainRules(Datasets dataset, Double consistencyThreshold) {
		String result = DEFAULT_RULES_RULEML_STRING;
		RuleSet rules = induceCertainRules(getObjects(dataset), consistencyThreshold);
		if (rules != null) {
			RuleMLBuilder ruleMLBuilder = new RuleMLBuilder();
			result = ruleMLBuilder.toRuleMLString(rules, DEFAULT_RULE_SET_ID);
		}
		return result;
	}
	
	/**
	 * Induces certain decision rules satisfying consistency threshold (in case when it is not {@code null}) on the basis of the provided data set.
	 * 
	 * @param informationTable {@link InformationTable} with objects on which rules are induced
	 * @param consistencyThreshold threshold on a consistency measure {@link ConsistencyMeasure}, if it is {@code null} then 
	 * 			consistency threshold is not taken into account 
	 * 
	 * @return certain decision rules as {@link RuleSet}
	 */
	private RuleSet induceCertainRules(InformationTable informationTable, Double consistencyThreshold) {
		RuleSet rules = null;
		if (informationTable != null) {
			VCDomLEMWrapper vcDomLEMWrapper = new VCDomLEMWrapper();
			if (consistencyThreshold != null) {
				rules = vcDomLEMWrapper.induceRulesWithCharacteristics(informationTable, consistencyThreshold);
			}
			else {
				rules = vcDomLEMWrapper.induceRulesWithCharacteristics(informationTable);
			}
		}
		return rules; 
	}
	
	/**
	 * Induces possible decision rules on the basis of the provided data set.
	 * 
	 * @param dataset data set {@link Datasets} on which rules are induced
	 * 
	 * @return possible decision rules in ruleML format
	 */
	private String inducePossibleRules(Datasets dataset) {
		String result = DEFAULT_RULES_RULEML_STRING;
		RuleSet rules = inducePossibleRules(getObjects(dataset));
		if (rules != null) {
			RuleMLBuilder ruleMLBuilder = new RuleMLBuilder();
			result = ruleMLBuilder.toRuleMLString(rules, DEFAULT_RULE_SET_ID);
		}
		return result;
	}
	
	/**
	 * Induces possible decision rules on the basis of the provided data set.
	 * 
	 * @param informationTable {@link InformationTable} with objects on which rules are induced
	 * 
	 * @return possible decision rules as {@link RuleSet}
	 */
	private RuleSet inducePossibleRules(InformationTable informationTable) {
		RuleSet rules = null;
		if (informationTable != null) {
			PossibleVCDomLEMWrapper vcDomLEMWrapper = new PossibleVCDomLEMWrapper();
			rules = vcDomLEMWrapper.induceRulesWithCharacteristics(informationTable);
		}
		return rules; 
	}
	
	/**
	 * Provides classifier {@link SimpleRuleClassifier} on the basis of the provided rule set {@link Rules}.
	 * 
	 * @param rules rules, which are used for classification
	 * @return classifier {@link SimpleRuleClassifier}
	 */
	private SimpleRuleClassifier getClassifier(Rules rules) {
		RuleSet ruleSet = null;
		SimpleClassificationResult defaultResponse = null;
		SimpleRuleClassifier classifier = null;
		
		if (rules != null) {
			Attribute [] attributes = getAttributes(rules.getMetadata());
			if (attributes != null) {
				ruleSet = getRuleSet(rules);
				if (ruleSet != null) {
					//set mean (median) value (according to the values of the first active enumeration decision attribute)
					for (int i = 0; i < attributes.length; i++) {
						if((attributes[i].isActive()) && (attributes[i] instanceof EvaluationAttribute)) {
							EvaluationAttribute attribute = (EvaluationAttribute)attributes[i];
							if ((attribute.getType() == AttributeType.DECISION) && (attribute.getValueType() instanceof EnumerationField)) {
								ElementList classLabels = ((EnumerationField)attribute.getValueType()).getElementList();
								defaultResponse = new SimpleClassificationResult(new SimpleDecision(EnumerationFieldFactory.getInstance().create(classLabels, 
										classLabels.getSize()/2, attribute.getPreferenceType()), i));
								logger.info("Set default response (rank): " + defaultResponse.getSuggestedDecision().getEvaluation(i));
								break;
							}
						}
					}
					classifier = new SimpleRuleClassifier(ruleSet, defaultResponse);
				}
			}			
		}
		
		return classifier;
	}
	
	/**
	 * Provides classifier {@link SimpleRuleClassifier} on the basis of the provided {@link RuleSet rule set} and array of {@link Attribute attributes}.
	 * 
	 * @param rules rules, which are used for classification
	 * @param attributes attributes describing rules
	 * 
	 * @return classifier {@link SimpleRuleClassifier}
	 */
	private SimpleRuleClassifier getClassifier(RuleSet rules, Attribute [] attributes) {
		SimpleClassificationResult defaultResponse = null;
		SimpleRuleClassifier classifier = null;
		
		if (rules != null) {
			if (attributes != null) {
				if (rules != null) {
					//set mean (median) value (according to the values of the first active enumeration decision attribute)
					for (int i = 0; i < attributes.length; i++) {
						if((attributes[i].isActive()) && (attributes[i] instanceof EvaluationAttribute)) {
							EvaluationAttribute attribute = (EvaluationAttribute)attributes[i];
							if ((attribute.getType() == AttributeType.DECISION) && (attribute.getValueType() instanceof EnumerationField)) {
								ElementList classLabels = ((EnumerationField)attribute.getValueType()).getElementList();
								defaultResponse = new SimpleClassificationResult(new SimpleDecision(EnumerationFieldFactory.getInstance().create(classLabels, 
										classLabels.getSize()/2, attribute.getPreferenceType()), i));
								logger.info("Set default response (rank): " + defaultResponse.getSuggestedDecision().getEvaluation(i));
								break;
							}
						}
					}
					classifier = new SimpleRuleClassifier(rules, defaultResponse);
				}
			}			
		}
		
		return classifier;
	}
	
	/**
	 * Initializes data base with default user and default model (if needed).
	 */
	@PostConstruct
	void initialization() {
		
		logger.info("DB Configuration: spring.datasource.url = " + environment.getProperty("spring.datasource.url"));
		logger.info("DB Configuration: spring.datasource.driver-class-name = " + environment.getProperty("spring.datasource.driver-class-name"));
		logger.info("DB Configuration: spring.datasource.username = " + environment.getProperty("spring.datasource.username"));
		
		logger.info("Post construct initialization started...");
		
		boolean store = false;
		String metadataText = "";
		String rulesText = "";
		if (appContext != null) {
			if (!usersRepository.findById(DEFAULT_USER_ID).isPresent()) {
	    		Resource resource = null;
	    	
	    		//load attribute meta-data
	    		AttributeParser attributeParser = new AttributeParser();
	    		Attribute [] attributes = null;
	    		resource = appContext.getResource("classpath:model/json/prioritisation.json");
	    		try {
	    			if (resource.exists()) {
	    				InputStreamReader reader = new InputStreamReader(resource.getInputStream());
	    				metadataText = IOUtils.toString(reader);
	    				reader.close();
	    				reader = new InputStreamReader(resource.getInputStream());
	    				attributes = attributeParser.parseAttributes(reader);
	    				reader.close();
	    				logger.info("Loaded meta-data of meta-alerts prioritisation: " + resource.getURL().toString());
	    			}
	    			else {
	    				logger.error("Attributes meta-data describing meta-alerts not available.");
	    				throw new ResourceAccessException("Attributes meta-data describing meta-alerts not available.");
	    			}
	    		}
	    		catch (IOException ex) {
	    			logger.error(ex.toString());
	    		}
	    		
	    		//load rule model
	    		if (attributes != null) {
	    			Map<Integer, RuleSet> allRules = null;
	    			RuleParser ruleParser = new RuleParser(attributes);
	    			
	    			resource = appContext.getResource("classpath:model/ruleml/prioritisation2.rules.xml");
	    			try {
	    				if (resource.exists()) {
	    					InputStream stream = resource.getInputStream();
	    					rulesText = IOUtils.toString(stream, StandardCharsets.UTF_8.name());
	    					stream.close();
	    					stream = resource.getInputStream();
	    					allRules = ruleParser.parseRules(stream);
	    					stream.close();
	    					logger.info("Loaded rule model of meta-alerts prioritisation: " + resource.getURL().toString());
	    				}
	    				else {
	    					logger.error("Rule model of meta-alerts prioritisation not available.");
	    					throw new ResourceAccessException("Rule model of meta-alerts prioritisation not available.");
	    				}
	    			}
	    			catch (IOException ex) {
	    				logger.error(ex.toString());
	    			}
	    			
	    			if ((allRules != null) && (allRules.size() > 0)) {
	    				store = true;
	    			}
	    			else {
	    				logger.error("Rule model of meta-alerts prioritisation cannot be loaded.");
	    				throw new ResourceAccessException("Rule model of meta-alerts prioritisation cannot be loaded.");
	    			}
	    		}
	    		else {
	    			logger.error("Attributes meta-data describing alerts cannot be loaded.");
	    			throw new ResourceAccessException("Attributes meta-data describing alerts cannot be loaded.");
	    		}
    		}
		}
		
		if (store) {
			// set data
			Users user = new Users();
			user.setId(DEFAULT_USER_ID);
			user.setName("PROTECTIVE");
			Metadata metadata = new Metadata();
			metadata.setName("protective default");
			metadata.setDescription("Default meta-data for priotisation in PROTECTIVE");
			metadata.setDate(Date.from(Instant.now()));
			metadata.setData(metadataText);
			Rules rules = new Rules();
			rules.setName("protective default");
			rules.setDescription("Default rules for priotisation in PROTECTIVE");
			rules.setDate(Date.from(Instant.now()));
			rules.setMetadata(metadata);
			rules.setData(rulesText);
			Models model = new Models();
			model.setName("protective default");
			model.setDescription("Default rules model for priotisation in PROTECTIVE");
			model.setDate(Date.from(Instant.now()));
			model.setUser(user);
			model.setRules(rules);
			
			// store data
			usersRepository.save(user);
			logger.info("Stored user with id: " + user.getId());
			logger.info("Stored user with name: " + user.getName());
			metaDataRepository.save(metadata);
			logger.info("Stored meta-data with id: " + metadata.getId());
			logger.info("Stored meta-data with date: " + metadata.getDate().toString());
			logger.info("Stored meta-data with name: " + metadata.getName());
			logger.info("Stored meta-data with description: " + metadata.getDescription());
			rulesRepository.save(rules);
			logger.info("Stored rules with id: " + rules.getId());
			logger.info("Stored rules with meta-data id: " + rules.getMetadata().getId());
			if (rules.getDataset() != null) {
				logger.info("Stored rules with data set id: " + rules.getDataset().getId());
			}
			logger.info("Stored rules with date: " + rules.getDate().toString());
			logger.info("Stored rules with name: " + rules.getName());
			logger.info("Stored rules with description: " + rules.getDescription());
			modelsRepository.save(model);
			logger.info("Stored model with id: " + model.getId());
			logger.info("Stored rules with date: " + model.getDate().toString());
			logger.info("Stored model with name: " + model.getName());
			logger.info("Stored model with description: " + model.getDescription());
			user.setActiveModel(model);
			usersRepository.save(user);
			logger.info("Stored active model with id: " + model.getId());
		}		    		
	}
	
	/**
	 * HTTP endpoint storing in a database meta-data provided as input.
	 *  
	 * @param input a JSON array representation of meta-data
	 * @param name name of the provided meta-data
	 * @param description description of the provided meta-data
	 * @return JSON representation of id in the database of the provided meta-data 
	 */
    @RequestMapping(value = "/put-metadata", method = {RequestMethod.PUT, RequestMethod.POST}, consumes = {"application/json;charset=UTF-8", "text/plain;charset=UTF-8"}, 
    					produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString putMetadata(@RequestBody String input, @RequestParam(value="name", required=false) String name, 
    			@RequestParam(value="description", required=false) String description) {
	
    		logger.info("Endpoint /put-metadata started...");
    	
    		long id = -1;
		boolean store = false;
		JSONString result = new JSONString("{}");
		
		// check and set meta-data
		Metadata metadata = new Metadata();
		if ((name != null) && (!name.isEmpty())) {
			name = name.replaceAll("\"", "");
			metadata.setName(name);
		}
		if ((description != null) && (!description.isEmpty())) {
			description = description.replaceAll("\"", "");
			metadata.setDescription(description);
		}
		if ((input != null) && (!input.isEmpty())) {
			metadata.setDate(Date.from(Instant.now()));
			metadata.setData(input);
			store = true;
		}
		else {
			logger.error("No valid input data.");
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No valid input data.");
		}
		
		// store meta-data (if valid)
		if (store) {
			metaDataRepository.save(metadata);
			id = metadata.getId();
			logger.info("Stored meta-data with id: " + id);
			logger.info("Stored meta-data with date: " + metadata.getDate().toString());
			logger.info("Stored meta-data with name: " + metadata.getName());
			logger.info("Stored meta-data with description: " + metadata.getDescription());
			//logger.info("Stored meta-data with data: " + metaData.getData());
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
		
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving meta-data from a database.
	 *  
	 * @param id ID of meta-data in database (when this ID is {@code null} user ID and/or model ID is taken) that is retrieved
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); meta-data from active model of the user is retrieved
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		meta-data from the model is retrieved
	 * @return JSON array representation of the meta-data 
	 */
    @RequestMapping(value = "/get-metadata", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString getMetadata(@RequestParam(value="id", required=false) Long id,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    		
		logger.info("Endpoint /get-metadata started...");
	
		Metadata metadata = getMetadataByIdOrModel(id, userId, modelId);
		JSONString result = new JSONString("{}");
		
		if (metadata != null) {
			result.setContent(metadata.getData());
			logger.info("Retrieved meta-data id: " + metadata.getId());
			//logger.info("Retrieved meta-data data: " + result);
		}
		else {
			logger.error("Requested meta-data is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested meta-data is not available.");
		}
		
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving and validating meta-data from a database. 
	 * 
	 * @param id ID of meta-data in database (when this ID is {@code null} user ID and/or model ID is taken) that is validated
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); meta-data from active model of the user is validated
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		meta-data from the model is validated
	 * @return JSON representation validation results 
	 */
    @RequestMapping(value = "/validate-metadata", produces = "application/json;charset=UTF-8")
    public @ResponseBody AttributesDescription validateMetadata(@RequestParam(value="id", required=false) Long id,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    		
    	logger.info("Endpoint /validate-metadata started...");
    	
    	Metadata metadata = getMetadataByIdOrModel(id, userId, modelId);
    	AttributesDescription attributesDescription = new AttributesDescription();
		
		if (metadata != null) {
			logger.info("Retrieved meta-data id: " + metadata.getId());
			AttributeParser attributeParser = new AttributeParser();
			Attribute [] attributes = null;
			try (StringReader attributeReader = new StringReader(metadata.getData())) {
				attributes = attributeParser.parseAttributes(attributeReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data: " + ex.toString());
			}
			if (attributes != null) {
				attributesDescription.setNumberOfAttributes(attributes.length);
			}
			else {
				logger.error("Error while parsing meta-data.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data.");
			}
			
		}
		else {
			logger.error("Requested meta-data is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested meta-data is not available.");
		}
		
		return attributesDescription;
    }
    
    /**
	 * HTTP endpoint storing, in a database, data set provided as input.
	 *  
	 * @param input a JSON array representation of a data set
	 * @param metadataId ID of meta-data in database (when this ID is {@code null} user ID and/or model ID is taken) describing stored data set
	 * @param name name of the stored data set
	 * @param description description of the stored data set
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); used to select meta-data describing stored data set
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		  used to select meta-data describing stored data set
	 * @return JSON representation of id in the database of the provided data set 
	 */
    @RequestMapping(value = "/put-dataset", method = {RequestMethod.PUT, RequestMethod.POST}, consumes = {"application/json;charset=UTF-8", "text/plain;charset=UTF-8"}, 
			produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString putDataSet(@RequestBody String input, 
    		@RequestParam(value="metadata_id", required=false) Long metadataId, 
    		@RequestParam(value="name", required=false) String name, 
    		@RequestParam(value="description", required=false) String description,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
		
    	logger.info("Endpoint /put-dataset started...");
    		
    	long id = -1;
		Metadata metadata = getMetadataByIdOrModel(metadataId, userId, modelId);
		Datasets dataSet = null;
		boolean store = false;
		JSONString result = new JSONString("{}");
				
		if (metadata != null) {
			logger.info("Retrieved meta-data id: " + metadata.getId());
			// construct data set
			dataSet = new Datasets();
			dataSet.setMetadata(metadata);
			if ((name != null) && (!name.isEmpty())) {
				name = name.replaceAll("\"", "");
				dataSet.setName(name);
			}
			if ((description != null) && (!description.isEmpty())) {
				description = description.replaceAll("\"", "");
				dataSet.setDescription(description);
			}
			if ((input != null) && (!input.isEmpty())) {
				dataSet.setDate(Date.from(Instant.now()));
				dataSet.setData(input);
				store = true;
			}
			else {
				logger.error("No valid input data.");
				throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No valid input data.");
			}
			
			// store data set (if valid)
			if (store) {
				dataSetsRepository.save(dataSet);
				id = dataSet.getId();
				logger.info("Stored data set with id: " + id);
				logger.info("Stored data set with meta-data id: " + dataSet.getMetadata().getId());
				logger.info("Stored data set with date: " + dataSet.getDate().toString());
				logger.info("Stored data set with name: " + dataSet.getName());
				logger.info("Stored data set with description: " + dataSet.getDescription());
				//logger.info("Stored data set with data: " + dataSet.getData());
			}
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
	
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving data set from a database.
	 *  
	 * @param id ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) that is retrieved
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); data set from active model of the user is retrieved
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		data set from the model is retrieved
	 * @return JSON array representation of the data set 
	 */
    @RequestMapping(value = "/get-dataset", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    	public @ResponseBody JSONString getDataSet(@RequestParam(value="id", required=false) Long id,
    			@RequestParam(value="user_id", required=false) String userId, 
        		@RequestParam(value="model_id", required=false) Long modelId) {		
    		
    		logger.info("Endpoint /get-dataset started...");
    		
    	Datasets dataSet = getDataSetByIdOrModel(id, userId, modelId);
    	JSONString result = new JSONString("{}");
		
		if (dataSet != null) {
			result.setContent(dataSet.getData());
			logger.info("Retrieved data set with id: " + dataSet.getId());
			logger.info("Retrieved data set with meta-data id: " + dataSet.getMetadata().getId());
			logger.info("Retrieved data set with date: " + dataSet.getDate().toString());
			logger.info("Retrieved data set with name: " + dataSet.getName());
			logger.info("Retrieved data set with description: " + dataSet.getDescription());
			//logger.info("Retrieved data set with data: " + result);
		}
		else {
			logger.error("Requested data set is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested data set is not available.");
		}
				
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving data set from a database and validating it. 
	 * 
	 * @param id ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) that is validated
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); data set from active model of the user is validated
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken); 
	 * 		;data set from the model is validated
	 * @param consistencyThreshold consistency threshold characterizing calculated lower approximations of unions of decision classes (should be in range [0.0, 1.0])
	 * 
	 * @return JSON representation validation results
	 */
    @RequestMapping(value = "/validate-dataset", produces = "application/json;charset=UTF-8")
    public @ResponseBody InformationTableDescription validateDataset(@RequestParam(value="id", required=false) Long id,
			@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId,
    		@RequestParam(value="consistency_threshold", required=false) Double consistencyThreshold) {
    		
    	logger.info("Endpoint /validate-dataset started...");
    	
    	Metadata metadata = null;
    	Datasets dataSet = getDataSetByIdOrModel(id, userId, modelId);
    	InformationTableDescription informationTableDescription = new InformationTableDescription();
		
		if (dataSet != null) {
			logger.info("Retrieved data set id: " + dataSet.getId());
			metadata = dataSet.getMetadata();
			logger.info("Retrieved meta-data id: " + metadata.getId());
			AttributeParser attributeParser = new AttributeParser();
			Attribute [] attributes = null;
			try (StringReader attributeReader = new StringReader(metadata.getData())) {
				attributes = attributeParser.parseAttributes(attributeReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data: " + ex.toString());
			}
			if (attributes != null) {
				ObjectParser objectParser = new ObjectParser.Builder(attributes).build();
				InformationTable informationTable = null;
				try (StringReader objectReader = new StringReader(dataSet.getData())) {
					informationTable = objectParser.parseObjects(objectReader);
				}
				catch (IOException ex) {
					logger.error(ex.toString());
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples: " + ex.toString());
				}
				if (informationTable != null) {
					informationTableDescription.setNumberOfObjects(informationTable.getNumberOfObjects());
					informationTableDescription.setNumberOfAttributes(informationTable.getNumberOfAttributes());
					if (informationTable.getDecisions() != null) {
						informationTableDescription.setActiveDecisionAttribute(true);
					}
					else {
						informationTableDescription.setActiveDecisionAttribute(false);
					}
					
					Decision[] orderedDecisions = informationTable.getOrderedUniqueFullyDeterminedDecisions();
					if (orderedDecisions != null) {
						// check names of attributes in decision and their values
						String [] decisionNames = new String[orderedDecisions.length]; 
						for (int i = 0; i < orderedDecisions.length; i++) {
							if (orderedDecisions[i].getNumberOfEvaluations() > 1) {
								StringBuilder decisionNamesBuilder = new StringBuilder();
								boolean first = true;
								for (int j : orderedDecisions[i].getAttributeIndices()) {
									if (first) {
										decisionNamesBuilder.append(informationTable.getAttribute(j).getName()).append("=").
										append(orderedDecisions[i].getEvaluation(j).toString());
										first = false;
									}
									else {
										decisionNamesBuilder.append(",").append(informationTable.getAttribute(j).getName()).append("=").
										append(orderedDecisions[i].getEvaluation(j).toString());
									}
								}
								decisionNames[i] = decisionNamesBuilder.toString();
							}
							else {
								for (int j : orderedDecisions[i].getAttributeIndices()) {
									decisionNames[i] = informationTable.getAttribute(j).getName() + "=" + orderedDecisions[i].getEvaluation(j).toString();
								}
							}
							//decisionNames[i] = orderedDecisions[i].toString();
						}
						informationTableDescription.setDecisions(decisionNames);
						// check if there are at least two different known values of of decision attribute in the processed data set
						if (orderedDecisions.length > 1) {
							// calculate unions
							Unions unions;
							try {
								if (consistencyThreshold != null) {
									unions = new UnionsWithSingleLimitingDecision(new InformationTableWithDecisionDistributions(informationTable), 
											new VCDominanceBasedRoughSetCalculator(EpsilonConsistencyMeasure.getInstance(), consistencyThreshold));
								}
								else {
									unions = new UnionsWithSingleLimitingDecision(new InformationTableWithDecisionDistributions(informationTable), 
											new ClassicalDominanceBasedRoughSetCalculator());
								}
							}
							catch (InvalidSizeException ex) {
								logger.error(ex.toString());
								throw new ResponseStatusException(
								          HttpStatus.BAD_REQUEST, "Error while calculating unions of decision classes: " + ex.toString());
							}
							Union[] downwardUnions = unions.getDownwardUnions(true);
							Union[] upwardUnions = unions.getUpwardUnions(true);
							// set union descriptions
							UnionDescription [] downwardUnionsDescription = new UnionDescription[downwardUnions.length];
							UnionDescription [] upwardUnionsDescription = new UnionDescription[upwardUnions.length];
							for (int i = 0; i < downwardUnionsDescription.length; i++) {
								UnionDescription unionDescription = new UnionDescription();
								unionDescription.setName(downwardUnions[i].toString());
								unionDescription.setUnionSize(downwardUnions[i].size());
								unionDescription.setLowerApproximationSize(downwardUnions[i].getLowerApproximation().size());
								downwardUnionsDescription[i] = unionDescription;
							}
							informationTableDescription.setDownwardUnionsDescription(downwardUnionsDescription);
							for (int i = 0; i < upwardUnionsDescription.length; i++) {
								UnionDescription unionDescription = new UnionDescription();
								unionDescription.setName(upwardUnions[i].toString());
								unionDescription.setUnionSize(upwardUnions[i].size());
								unionDescription.setLowerApproximationSize(upwardUnions[i].getLowerApproximation().size());
								upwardUnionsDescription[i] = unionDescription;
							}
							informationTableDescription.setUpwardUnionsDescription(upwardUnionsDescription);
						}
					}
				}
				else{
					logger.error("Error while parsing data set.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing data set.");
				}
			}
			else {
				logger.error("Error while parsing meta-data.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data.");
			}
			
		}
		else {
			logger.error("Requested data set is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested data set is not available.");
		}
		
		return informationTableDescription;
    }

    /**
	 * HTTP endpoint storing, in a database, rules provided as input.
	 *  
	 * @param input a ruleML representation of rules
	 * @param metadataId ID of meta-data in database (when this ID is {@code null} user ID and/or model ID is taken) describing the provided data set and rules, which 
	 * 		are passed as input
	 * @param dataSetId ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) associated with rules, which 
	 * 		are passed as input
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		meta-data and data set from active model of the user are associated with rules, which are passed as input
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		meta-data and data set from the model are associated with rules, which are passed as input
	 * @param name name of the provided rules
	 * @param description description of the provided rules
	 * @return JSON representation of id in the database of the provided rules 
	 */
    @RequestMapping(value = "/put-rules", method = {RequestMethod.PUT, RequestMethod.POST}, consumes = {"text/plain;charset=UTF-8"}, 
			produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString putRules(@RequestBody String input, 
    		@RequestParam(value="metadata_id", required=false) Long metadataId, 
    		@RequestParam(value="dataset_id", required=false) Long dataSetId, 
    		@RequestParam(value="name", required=false) String name, 
			@RequestParam(value="description", required=false) String description,
			@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    	
    	logger.info("Endpoint /put-rules started...");
    	
		long id = -1;
		Metadata metadata = getMetadataByIdOrModel(metadataId, userId, modelId);
		Datasets dataSet = getDataSetByIdOrModel(dataSetId, userId, modelId);
		Rules rules = null;
		boolean store = false;
		JSONString result = new JSONString("{}");
				
		if ((metadata != null) || (dataSet != null)) {
			if (metadata == null) {
				metadata = dataSet.getMetadata();
			}
			logger.info("Retrieved meta-data id: " + metadata.getId());
			// construct rules
			rules = new Rules();
			rules.setMetadata(metadata);
			if (dataSet != null) {
				logger.info("Retrieved data set id: " + dataSet.getId());
				rules.setDataset(dataSet);
			}
			if ((name != null) && (!name.isEmpty())) {
				name = name.replaceAll("\"", "");
				rules.setName(name);
			}
			if ((description != null) && (!description.isEmpty())) {
				description = description.replaceAll("\"", "");
				rules.setDescription(description);
			}
			if ((input != null) && (!input.isEmpty())) {
				rules.setDate(Date.from(Instant.now()));
				rules.setData(input);
				store = true;
			}
			else {
				logger.error("No valid input data.");
				throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No valid input data.");
			}
			
			// store rules (if valid)
			if (store) {
				rulesRepository.save(rules);
				id = rules.getId();
				logger.info("Stored rules with id: " + id);
				logger.info("Stored rules with meta-data id: " + rules.getMetadata().getId());
				if (rules.getDataset() != null) {
					logger.info("Stored rules with data set id: " + rules.getDataset().getId());
				}
				logger.info("Stored rules with date: " + rules.getDate().toString());
				logger.info("Stored rules with name: " + rules.getName());
				logger.info("Stored rules with description: " + rules.getDescription());
				//logger.info("Stored rules with data: " + rules.getData());
			}
		}
		else {
			logger.error("Requested meta-data is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested meta-data is not available.");
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
	
		return result;
    }
    
    /**
	 * HTTP endpoint generating (learning) decision rules and storing them in a database.
	 *  
	 * @param dataSetId ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) associated with rules, which 
	 * 		are generated
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		meta-data and data set from active model of the user are associated with rules, which are generated
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		meta-data and data set from the model are associated with rules, which are generated
	 * @param name name of the generated rules
	 * @param description description of the generated rules
	 * @param rulesType type of induced rules; when equals {@code possible} then possible rules are induced, otherwise certain rules are induced
	 * @param consistencyThreshold consistency threshold characterizing generated rules (should be in range [0.0, 1.0])
	 * @return JSON representation of id in the database of the generated rules 
	 */
    @RequestMapping(value = "/learn", produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString induceRules( 
    		@RequestParam(value="dataset_id", required=false) Long dataSetId, 
    		@RequestParam(value="name", required=false) String name, 
			@RequestParam(value="description", required=false) String description,
			@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId,
    		@RequestParam(value="rules_type", required=false) String rulesType,
    		@RequestParam(value="consistency_threshold", required=false) Double consistencyThreshold) {
    	
    	logger.info("Endpoint /learn started...");
    	
		long id = -1;
		Datasets dataSet = getDataSetByIdOrModel(dataSetId, userId, modelId);
		Rules rules = null;
		boolean store = false;
		JSONString result = new JSONString("{}");
				
		if (dataSet != null) {
			logger.info("Retrieved meta-data id: " + dataSet.getMetadata().getId());
			// construct rules
			rules = new Rules();
			rules.setMetadata(dataSet.getMetadata());
			if (dataSet != null) {
				logger.info("Retrieved data set id: " + dataSet.getId());
				rules.setDataset(dataSet);
			}
			if ((name != null) && (!name.isEmpty())) {
				name = name.replaceAll("\"", "");
				rules.setName(name);
			}
			if ((description != null) && (!description.isEmpty())) {
				description = description.replaceAll("\"", "");
				rules.setDescription(description);
			}
			String inducedRules = null;
			if ((rulesType != null) && (rulesType.equalsIgnoreCase("possible"))) {
				inducedRules = inducePossibleRules(dataSet);
			}
			else if (consistencyThreshold != null) {
				if ((consistencyThreshold >= 0.0) && (consistencyThreshold <= 1.0)) {
					inducedRules = induceCertainRules(dataSet, consistencyThreshold);
				}
				else {
					logger.error("Incorrect value of consistency threshold.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect value of consistency threshold.");
				}
			}
			else {
				inducedRules = induceCertainRules(dataSet, consistencyThreshold);
			}
			
			if ((inducedRules != null) && (!inducedRules.isEmpty())) {
				rules.setDate(Date.from(Instant.now()));
				rules.setData(inducedRules);
				store = true;
			}
			else {
				logger.error("Error while generating decision rules.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while generating decision rules.");
			}
			
			// store rules (if valid)
			if (store) {
				rulesRepository.save(rules);
				id = rules.getId();
				if ((rulesType != null) && (rulesType.equalsIgnoreCase("possible"))) {
					logger.info("Possible rules were induced");
				}
				else if (consistencyThreshold != null) {
					logger.info("Certain rules with consistency threshold " + consistencyThreshold + " were induced");
				}
				else {
					logger.info("Certain rules were induced.");
				}
				logger.info("Stored rules with id: " + id);
				logger.info("Stored rules with meta-data id: " + rules.getMetadata().getId());
				if (rules.getDataset() != null) {
					logger.info("Stored rules with data set id: " + rules.getDataset().getId());
				}
				logger.info("Stored rules with date: " + rules.getDate().toString());
				logger.info("Stored rules with name: " + rules.getName());
				logger.info("Stored rules with description: " + rules.getDescription());
				//logger.info("Stored rules with data: " + rules.getData());
			}
		}
		else {
			logger.error("Requested data set is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested data set is not available.");
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
	
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving rules from a database.
	 * 
	 * @param id id of rules in the database
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		rules from active model of the user are retrieved
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		rules from the model are retrieved
	 * @return ruleML representation of the the rules 
	 */
    @RequestMapping(value = "/get-rules", method = RequestMethod.GET, produces = "application/xml;charset=UTF-8")
    public @ResponseBody String getRules(@RequestParam(value="id", required=false) Long id,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {		
    		
    	logger.info("Endpoint /get-rules started...");
    	
    	Rules rules = null;
    	String result = DEFAULT_RULES_RULEML_STRING;
    	
    	rules = getRulesByIdOrModel(id, userId, modelId);
		
		if (rules != null) {
			result = rules.getData();
			logger.info("Retrieved rules with id: " + rules.getId());
			logger.info("Retrieved rules with meta-data id: " + rules.getMetadata().getId());
			if (rules.getDataset() != null) {
				logger.info("Retrieved rules with data set id: " + rules.getDataset().getId());
			}
			logger.info("Retrieved rules with date: " + rules.getDate().toString());
			logger.info("Retrieved rules with name: " + rules.getName());
			logger.info("Retrieved rules with description: " + rules.getDescription());
			//logger.info("Retrieved rules with data: " + rules.getData());
		}
		else {
			logger.error("Requested rules are not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested rules are not available.");
		}
		
		return result;
    }
    
    /**
	 * HTTP endpoint retrieving rules from a database and validating them.
	 * 
	 * @param id id of rules in the database
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		rules from active model of the user are validated
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		rules from the model are validated
	 * @return JSON representation validation results
	 */
    @RequestMapping(value = "/validate-rules", produces = "application/json;charset=UTF-8")
    public @ResponseBody RulesDescription validateRules(@RequestParam(value="id", required=false) Long id,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    		
    	logger.info("Endpoint /validate-rules started...");
    	
    	Rules rules = getRulesByIdOrModel(id, userId, modelId);
		RulesDescription rulesDescription = new RulesDescription();
	
		if (rules != null) {
			logger.info("Retrieved meta-data with id: " + rules.getMetadata().getId());
			logger.info("Retrieved rules with id: " + rules.getId());
			// check whether meta-data and rules can be parsed
			AttributeParser attributeParser = new AttributeParser();
			Attribute [] attributes = null;
			try (StringReader attributeReader = new StringReader(rules.getMetadata().getData())) {
				attributes = attributeParser.parseAttributes(attributeReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data: " + ex.toString());
			}
			if (attributes != null) {
				Map<Integer, RuleSet> allRules = null;
				RuleParser ruleParser = new RuleParser(attributes);
				try (InputStream rulesStream = IOUtils.toInputStream(rules.getData(), StandardCharsets.UTF_8.name())) {
					allRules = ruleParser.parseRules(rulesStream);
				}
				catch (IOException ex) {
					logger.error(ex.toString());
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing rules: "+ex.toString());
				}
				if ((allRules != null) && (allRules.size() > 0)) {
					rulesDescription.setNumberOfRules(((RuleSet)allRules.get(1)).size());
				}
				else {
					logger.error("Error while parsing rules.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing rules.");
				}
			}
			else {
				logger.error("Error while parsing meta-data.");	
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data.");
			}
		}
		else {
			logger.error("Requested rules are not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested rules are not available.");
		}
		
		return rulesDescription;
    }
    
    /**
	 * HTTP endpoint validating classification on validation set with rules induced on learning set. Both validation and learning sets are subsets of data set, 
	 * which is stored in database. This data set is split into validation and learning sets according to proportions passed as parameters. Sum of proportions 
	 * does not need to be 1.0 but it should not be greater than 1.0.
	 *  
	 * @param dataSetId ID of data set in database (when this ID is {@code null} user ID and/or model ID is taken) associated with rules, which 
	 * 		are generated
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		meta-data and data set from active model of the user are associated with rules, which are generated
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		meta-data and data set from the model are associated with rules, which are generated
	 * @param rulesType type of induced rules; when equals {@code possible} then possible rules are induced, otherwise certain rules are induced
	 * @param consistencyThreshold consistency threshold characterizing generated rules (should be in range [0.0, 1.0])
	 * @param learningProportion proportion of data set used for learning should be in range (0.0, 1.0)
	 * @param validationProportion proportion of data set used for validation should be in range (0.0, 1.0)
	 * @param seed seed for random numbers generator
	 * 
	 * @return JSON representation validation results
	 */
    @RequestMapping(value = "/validate", produces = "application/json;charset=UTF-8")
    public @ResponseBody ClassificationTestResult validateModel( 
    		@RequestParam(value="dataset_id", required=false) Long dataSetId, 
			@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId,
    		@RequestParam(value="rules_type", required=false) String rulesType,
    		@RequestParam(value="consistency_threshold", required=false) Double consistencyThreshold,
    		@RequestParam(value="learning_proportion", required=false) Double learningProportion,
    		@RequestParam(value="validation_proportion", required=false) Double validationProportion,
    		@RequestParam(value="seed", required=false) Long seed) {
    	
    	logger.info("Endpoint /validate started...");
    	
		Datasets dataSet = getDataSetByIdOrModel(dataSetId, userId, modelId);
		ClassificationTestResult classificationTestResult = new ClassificationTestResult();
				
		if (dataSet != null) {
			logger.info("Retrieved data set with id: " + dataSet.getId());
			logger.info("Retrieved meta-data id: " + dataSet.getMetadata().getId());
			
			// split data set into learning and validation subsets
			Random random;
			List<InformationTable> informationTables = null;
			if ((learningProportion != null) && (validationProportion != null)) {
				if (seed != null) {
					random = new Random(seed); 
				}
				else {
					random = new Random();
				}
				try {
					informationTables = Splitter.randomStratifiedSplit(new InformationTableWithDecisionDistributions(getObjects(dataSet), true),
						true, random, new double [] {learningProportion, validationProportion});
				}
				catch (IllegalArgumentException ex) {
					logger.error(ex.toString());
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while splitting data: " + ex.toString());
				}
				catch (InvalidValueException ex) {
					logger.error(ex.toString());
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while splitting data: " + ex.toString());
				}
			}
			else {
				logger.error("Proportions for splitting data set into learning and validation sets were not given.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Proportions for splitting data set into learning and validation sets were not given.");
			}

			// induce rules
			RuleSet rules = null;
			if ((informationTables != null) && (!informationTables.isEmpty())) {
				if ((rulesType != null) && (rulesType.equalsIgnoreCase("possible"))) {
					rules = inducePossibleRules(informationTables.get(0));
				}
				else if (consistencyThreshold != null) {
					if ((consistencyThreshold >= 0.0) && (consistencyThreshold <= 1.0)) {
						rules = induceCertainRules(informationTables.get(0), consistencyThreshold);
					}
					else {
						logger.error("Incorrect value of consistency threshold.");
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect value of consistency threshold.");
					}
				}
				else {
					rules = induceCertainRules(informationTables.get(0), consistencyThreshold);
				}
			}
			else {
				logger.error("Error while splitting data set into learning and validation sets.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while splitting data set into learning and validation sets.");
			}
			
			// validate rule model
			if ((rules != null)) {
				InformationTable informationTable = informationTables.get(1);
				if (informationTable != null) {
					SimpleRuleClassifier classifier = getClassifier(rules, informationTable.getAttributes());
					ClassificationResult[] classificationResults = new ClassificationResult[informationTable.getNumberOfObjects()];
					Decision [] assignedDecisions = new Decision[informationTable.getNumberOfObjects()];
					
					for (int i = 0; i < informationTable.getNumberOfObjects(); i++) {
						classificationResults[i] = classifier.classify(i, informationTable);
						assignedDecisions[i] = classificationResults[i].getSuggestedDecision();
					}
					// set results of validation
					ClassificationValidationResult validatationResult = new ClassificationValidationResult(informationTable.getDecisions(), classificationResults);
					classificationTestResult.setNumberOfConsistentAssignments((int)validatationResult.getNumberOfConsistentAssignments());
					classificationTestResult.setNumberOfCorrectAssignments((int)validatationResult.getNumberOfCorrectAssignments());
					classificationTestResult.setNumberOfInconsistentAssignments((int)validatationResult.getNumberOfInconsistentAssignments());
					classificationTestResult.setNumberOfIncorrectAssignments((int)validatationResult.getNumberOfIncorrectAssignments());
					// set accuracy, G-mean, MAE, and RMSE
					Decision [] decisions = informationTable.getOrderedUniqueFullyDeterminedDecisions();
					TruePositiveRateDescription [] truePositiveRateDescriptions = new TruePositiveRateDescription[decisions.length]; 
					OrdinalMisclassificationMatrix misclassificationMatrix = new OrdinalMisclassificationMatrix(informationTable.getDecisions(), assignedDecisions, 
							informationTable.getOrderedUniqueFullyDeterminedDecisions());
					classificationTestResult.setClassificationAccuracy(misclassificationMatrix.getAccuracy());
					classificationTestResult.setGMean(misclassificationMatrix.getGmean());
					classificationTestResult.setMae(misclassificationMatrix.getMAE());
					classificationTestResult.setRmse(misclassificationMatrix.getRMSE());
					// set true positive rates
					for (int i = 0; i < decisions.length; i++) {
						TruePositiveRateDescription truePositiveRateDescription = new TruePositiveRateDescription();
						truePositiveRateDescription.setDecision(decisions[i].toString());
						truePositiveRateDescription.setValue(misclassificationMatrix.getTruePositiveRate(decisions[i]));
						//logger.info("TPR for " + truePositiveRateDescription.getDecision() + " = " + truePositiveRateDescription.getValue());
						truePositiveRateDescriptions[i] = truePositiveRateDescription;
					}
					classificationTestResult.setTruePositiveRates(truePositiveRateDescriptions);				
				}
				
			}
			else {
				logger.error("Error while inducing decision rules.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while inducing decision rules.");
			}
		}
		else {
			logger.error("Requested data set is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested data set is not available.");
		}
		
		return classificationTestResult;
    }
    
    /**
	 * HTTP endpoint testing classification with rules on a data set; both rules and data set are retrieved from a database.
	 * 
	 * @param rulesId ID of rules in the database
	 * @param dataSetId ID of data set in the database
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken); 
	 * 		rules from active model of the user are tested
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken);
	 * 		rules from the model are tested
	 * @return JSON representation test results
	 */
    @RequestMapping(value = "/test", produces = "application/json;charset=UTF-8")
    public @ResponseBody ClassificationTestResult testModel(@RequestParam(value="rules_id", required=false) Long rulesId,
    		@RequestParam(value="dataset_id", required=false) Long dataSetId,
    		@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    		
    	logger.info("Endpoint /test started...");
    	
    	Rules rules = getRulesByIdOrModel(rulesId, userId, modelId);
    	Datasets dataSet = getDataSetByIdOrModel(dataSetId, userId, modelId);
		ClassificationTestResult classificationTestResult = new ClassificationTestResult();
	
		if (rules != null) { 
			if (dataSet != null) {
				logger.info("Retrieved meta-data with id: " + rules.getMetadata().getId());
				logger.info("Retrieved data set with id: " + dataSet.getId());
				logger.info("Retrieved rules with id: " + rules.getId());
				
				InformationTable informationTable = getObjects(dataSet);
				if (informationTable != null) {
					SimpleRuleClassifier classifier = getClassifier(rules);
					ClassificationResult[] classificationResults = new ClassificationResult[informationTable.getNumberOfObjects()];
					Decision [] assignedDecisions = new Decision[informationTable.getNumberOfObjects()];
					
					for (int i = 0; i < informationTable.getNumberOfObjects(); i++) {
						classificationResults[i] = classifier.classify(i, informationTable);
						assignedDecisions[i] = classificationResults[i].getSuggestedDecision();
					}
					// set results of validation
					ClassificationValidationResult validatationResult = new ClassificationValidationResult(informationTable.getDecisions(), classificationResults);
					classificationTestResult.setNumberOfConsistentAssignments((int)validatationResult.getNumberOfConsistentAssignments());
					classificationTestResult.setNumberOfCorrectAssignments((int)validatationResult.getNumberOfCorrectAssignments());
					classificationTestResult.setNumberOfInconsistentAssignments((int)validatationResult.getNumberOfInconsistentAssignments());
					classificationTestResult.setNumberOfIncorrectAssignments((int)validatationResult.getNumberOfIncorrectAssignments());
					// set accuracy, G-mean, MAE, and RMSE
					Decision [] decisions = informationTable.getOrderedUniqueFullyDeterminedDecisions();
					TruePositiveRateDescription [] truePositiveRateDescriptions = new TruePositiveRateDescription[decisions.length]; 
					OrdinalMisclassificationMatrix misclassificationMatrix = new OrdinalMisclassificationMatrix(informationTable.getDecisions(), assignedDecisions, 
							informationTable.getOrderedUniqueFullyDeterminedDecisions());
					classificationTestResult.setClassificationAccuracy(misclassificationMatrix.getAccuracy());
					classificationTestResult.setGMean(misclassificationMatrix.getGmean());
					classificationTestResult.setMae(misclassificationMatrix.getMAE());
					classificationTestResult.setRmse(misclassificationMatrix.getRMSE());
					// set true positive rates
					for (int i = 0; i < decisions.length; i++) {
						TruePositiveRateDescription truePositiveRateDescription = new TruePositiveRateDescription();
						truePositiveRateDescription.setDecision(decisions[i].toString());
						truePositiveRateDescription.setValue(misclassificationMatrix.getTruePositiveRate(decisions[i]));
						//logger.info("TPR for " + truePositiveRateDescription.getDecision() + " = " + truePositiveRateDescription.getValue());
						truePositiveRateDescriptions[i] = truePositiveRateDescription;
					}
					classificationTestResult.setTruePositiveRates(truePositiveRateDescriptions);				
				}
			}
			else {
				logger.error("Requested data set is not available.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested data set is not available.");
			}
		}
		else {
			logger.error("Requested rules are not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Requested rules are not available.");
		}
		
		return classificationTestResult;
    }
    
    /**
	 * HTTP endpoint adding a user to a database.
	 * 
	 * @param userId login of the user
	 * @param name name of the user
	 * @return JSON representation of id in the database of the added user 
	 */
    @RequestMapping(value = "/add-user", produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString addUser(@RequestParam(value="id", required=true) String userId, 
    		@RequestParam(value="name", required=true) String name) {
    	
    		logger.info("Endpoint /add-user started...");
    	
		boolean store = false;
		String id = "";
		JSONString result = new JSONString("{}") ;
		
		// check and set user
		Users user = new Users();
		if ((userId != null) && (!userId.isEmpty())) {
			userId = userId.replaceAll("\"", "");
			
			// check if login exists
			if (usersRepository.findById(userId).isPresent()) {
				logger.info("User login exists: " + userId);
				id = userId;
			}
			else {
				user.setId(userId);
				if ((name != null) && (!name.isEmpty())) {
					name = name.replaceAll("\"", "");
					user.setName(name);
					id = userId;
					store = true;
				}
			}
		}
		
		// store user (if valid)
		if (store) {
			usersRepository.save(user);
			logger.info("Stored user with id: " + user.getId());
			logger.info("Stored user with name: " + user.getName());
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
		
		return result;
    }
    
    /**
	 * HTTP endpoint storing in a database a rules model for the given user.
	 * 
	 * @param userId login of the user
	 * @param rulesId id of rules to be used in the rules model 
	 * @param name name of the rules model
	 * @param description description of the rule model
	 * @return JSON representation of id in the database of the rule model 
	 */
    @RequestMapping(value = "/set-rules-model", produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString setRulesModel(@RequestParam(value="user_id", required=true) String userId,
    		@RequestParam(value="rules_id", required=true) Long rulesId,
    		@RequestParam(value="name", required=false) String name, 
    		@RequestParam(value="description", required=false) String description) {
    	
    		logger.info("Endpoint /set-rules-model started...");
    	
		boolean store = false;
		long id = -1;
		Users user = null;
		Rules rules = null;
		JSONString result = new JSONString("{}");
		
		// check whether model is valid
		if ((userId != null) && (!userId.isEmpty())) {
			userId = userId.replaceAll("\"", "");	
			// check if login exists
			if (usersRepository.findById(userId).isPresent()) {
				user = usersRepository.findById(userId).get();
				logger.info("Retrieved user with id: " + user.getId());
				// check if the given rules exist
				if (rulesRepository.findById(rulesId).isPresent()) {
					rules = rulesRepository.findById(rulesId).get();
					logger.info("Retrieved meta-data with id: " + rules.getMetadata().getId());
					logger.info("Retrieved rules with id: " + rules.getId());
					// check whether meta-data and rules can be parsed
					AttributeParser attributeParser = new AttributeParser();
					Attribute [] attributes = null;
					try (StringReader attributeReader = new StringReader(rules.getMetadata().getData())) {
						attributes = attributeParser.parseAttributes(attributeReader);
					}
					catch (IOException ex) {
						logger.error(ex.toString());
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data: " + ex.toString());
					}
					if (attributes != null) {
						Map<Integer, RuleSet> allRules = null;
						RuleParser ruleParser = new RuleParser(attributes);
						try (InputStream rulesStream = IOUtils.toInputStream(rules.getData(), StandardCharsets.UTF_8.name())) {
							allRules = ruleParser.parseRules(rulesStream);
						}
						catch (IOException ex) {
							logger.error(ex.toString());
							throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing rules: "+ex.toString());
						}
						if ((allRules != null) && (allRules.size() > 0)) {
							store = true;
						}
						else {
							logger.error("Error while parsing rules.");
							throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing rules.");
						}
					}
					else {
						logger.error("Error while parsing meta-data.");
						throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error while parsing meta-data.");
					}
				}
				else {
					logger.error("Rules with requested ID are not available.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rules with requested ID are not available.");
				}
			}
			else {
				logger.error("User with requested ID is not available.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with requested ID is not available.");
			}
		}
		else {
			logger.error("User with requested ID is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with requested ID is not available.");
		}
		
		
		// store model
		if (store) {
			Models model = new Models();
			model.setUser(user);
			model.setRules(rules);
			if ((name != null) && (!name.isEmpty())) {
				name = name.replaceAll("\"", "");
				rules.setName(name);
			}
			if ((description != null) && (!description.isEmpty())) {
				description = description.replaceAll("\"", "");
				rules.setDescription(description);
			}
			model.setDate(Date.from(Instant.now()));
			modelsRepository.save(model);
			id = model.getId();
			logger.info("Stored model with id: " + id);
			logger.info("Stored rules with date: " + model.getDate().toString());
			logger.info("Stored model with name: " + model.getName());
			logger.info("Stored model with description: " + model.getDescription());
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
		
		return result;
    }
    
    /**
	 * HTTP endpoint setting an active model for the given user.
	 * 
	 * @param userId login of the user
	 * @param modelId id of model to be used as the active model 
	 * @return JSON representation of id in the database of the active model 
	 */
    @RequestMapping(value = "/set-active-model", produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString setActiveModel(@RequestParam(value="user_id", required=true) String userId,
    		@RequestParam(value="model_id", required=true) Long modelId) {
    	
    		logger.info("Endpoint /set-active-model started...");
    		
		boolean store = false;
		long id = -1;
		Users user = null;
		Models model = null;
		JSONString result = new JSONString("{}");
		
		if ((userId != null) && (!userId.isEmpty())) {
			userId = userId.replaceAll("\"", "");	
			// check if login exists
			if (usersRepository.findById(userId).isPresent()) {
				user = usersRepository.findById(userId).get();
				logger.info("Retrieved user with id: " + user.getId());
				// check if the given model exist
				if (modelsRepository.findById(modelId).isPresent()) {
					model = modelsRepository.findById(modelId).get();
					id = model.getId();
					logger.info("Retrieved model with id: " + id);
					store = true;
				}
				else {
					logger.error("Model with requested ID is not available.");
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Model with requested ID is not available.");
				}
			}
			else {
				logger.error("User with requested ID is not available.");
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with requested ID is not available.");
			}
		}
		else {
			logger.error("User with requested ID is not available.");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with requested ID is not available.");
		}
		
		// store active model
		if (store) {
			user.setActiveModel(model);
			usersRepository.save(user);
			logger.info("Stored active model with id: " + id);
		}
		
		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
		
		return result;
    }
    
    /**
	 * HTTP endpoint initializing a data base with the default user and the default active model. 
	 * 
	 * @return JSON representation of id in the database of the default active model  
	 */
    @RequestMapping(value = "/init", produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString init() {
    	
    		logger.info("Endpoint /init started...");
    		
    		long id = -1;
    		boolean store = false;
    		String metadataText = "";
    		String rulesText = "";
    		JSONString result = new JSONString("{}");
    		
    		if (appContext != null) {
    			if (usersRepository.findById(DEFAULT_USER_ID).isPresent()) {
    				logger.error("Default user protective and default model already defined.");
    				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Default user protective and default model already defined.");
    			}
    			else {
		    		Resource resource = null;
		    	
		    		//load attribute meta-data
		    		AttributeParser attributeParser = new AttributeParser();
		    		Attribute [] attributes = null;
		    		resource = appContext.getResource("classpath:model/json/prioritisation.json");
		    		try {
		    			if (resource.exists()) {
		    				InputStreamReader reader = new InputStreamReader(resource.getInputStream());
		    				metadataText = IOUtils.toString(reader);
		    				reader.close();
		    				reader = new InputStreamReader(resource.getInputStream());
		    				attributes = attributeParser.parseAttributes(reader);
		    				reader.close();
		    				logger.info("Loaded meta-data of alerts prioritisation: " + resource.getURL().toString());
		    			}
		    			else {
		    				logger.error("Attributes meta-data describing alerts not available.");
		    				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Attributes meta-data describing alerts not available.");
		    			}
		    		}
		    		catch (IOException ex) {
		    			logger.error(ex.toString());
		    			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Attributes meta-data describing alerts cannot be loaded.");
		    		}
		    		
		    		//load rule model
		    		if (attributes != null) {
		    			Map<Integer, RuleSet> allRules = null;
		    			RuleParser ruleParser = new RuleParser(attributes);
		    			
		    			resource = appContext.getResource("classpath:model/ruleml/prioritisation2.rules.xml");
		    			try {
		    				if (resource.exists()) {
		    					InputStream stream = resource.getInputStream();
		    					rulesText = IOUtils.toString(stream, StandardCharsets.UTF_8.name());
		    					stream.close();
		    					stream = resource.getInputStream();
		    					allRules = ruleParser.parseRules(stream);
		    					stream.close();
		    					logger.info("Loaded rule model of alerts prioritisation: " + resource.getURL().toString());
		    				}
		    				else {
		    					logger.error("Rule model of alerts prioritisation not available.");
		    					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rule model of alerts prioritisation not available.");
		    				}
		    			}
		    			catch (IOException ex) {
		    				logger.error(ex.toString());
		    				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rule model of alerts prioritisation cannot be loaded.");
		    			}
		    			
		    			if ((allRules != null) && (allRules.size() > 0)) {
		    				store = true;
		    			}
		    			else {
		    				logger.error("Rule model of alerts prioritisation cannot be loaded.");
		    				throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Rule model of alerts prioritisation cannot be loaded.");
		    			}
		    		}
		    		else {
		    			logger.error("Attributes meta-data describing alerts cannot be loaded.");
		    			throw new ResourceAccessException("Attributes meta-data describing alerts cannot be loaded.");
		    		}
	    		}
    		}
    		
    		if (store) {
    			// set data
    			Users user = new Users();
    			user.setId(DEFAULT_USER_ID);
    			user.setName("PROTECTIVE");
    			Metadata metadata = new Metadata();
    			metadata.setName("protective default");
    			metadata.setDescription("Default meta-data for priotisation in PROTECTIVE");
    			metadata.setDate(Date.from(Instant.now()));
    			metadata.setData(metadataText);
    			Rules rules = new Rules();
    			rules.setName("protective default");
    			rules.setDescription("Default rules for priotisation in PROTECTIVE");
    			rules.setDate(Date.from(Instant.now()));
    			rules.setMetadata(metadata);
    			rules.setData(rulesText);
    			Models model = new Models();
    			model.setName("protective default");
    			model.setDescription("Default rules model for priotisation in PROTECTIVE");
    			model.setDate(Date.from(Instant.now()));
    			model.setUser(user);
    			model.setRules(rules);
    			
    			// store data
    			usersRepository.save(user);
    			logger.info("Stored user with id: " + user.getId());
    			logger.info("Stored user with name: " + user.getName());
    			metaDataRepository.save(metadata);
    			logger.info("Stored meta-data with id: " + metadata.getId());
    			logger.info("Stored meta-data with date: " + metadata.getDate().toString());
    			logger.info("Stored meta-data with name: " + metadata.getName());
    			logger.info("Stored meta-data with description: " + metadata.getDescription());
    			rulesRepository.save(rules);
    			logger.info("Stored rules with id: " + rules.getId());
				logger.info("Stored rules with meta-data id: " + rules.getMetadata().getId());
				if (rules.getDataset() != null) {
					logger.info("Stored rules with data set id: " + rules.getDataset().getId());
				}
				logger.info("Stored rules with date: " + rules.getDate().toString());
				logger.info("Stored rules with name: " + rules.getName());
				logger.info("Stored rules with description: " + rules.getDescription());
    			modelsRepository.save(model);
    			id = model.getId();
    			logger.info("Stored model with id: " + id);
    			logger.info("Stored rules with date: " + model.getDate().toString());
    			logger.info("Stored model with name: " + model.getName());
    			logger.info("Stored model with description: " + model.getDescription());
    			user.setActiveModel(model);
    			usersRepository.save(user);
    			logger.info("Stored active model with id: " + model.getId());
    		}
    		
    		result.setContent(new StringBuilder().append("{\"id\":").append(id).append("}").toString());
    		    		
    		return result;
    }
}
