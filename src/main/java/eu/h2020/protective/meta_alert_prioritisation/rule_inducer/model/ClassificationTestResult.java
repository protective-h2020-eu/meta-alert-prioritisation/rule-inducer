/**
 * Copyright (C) Jerzy Błaszczyński, Marcin Szeląg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.h2020.protective.meta_alert_prioritisation.rule_inducer.model;

import lombok.Data;

/**
 * JSON representation of results of classification.
 *
 * @author Jerzy Błaszczyński (<a href="mailto:jurek.blaszczynski@cs.put.poznan.pl">jurek.blaszczynski@cs.put.poznan.pl</a>)
 * @author Marcin Szeląg (<a href="mailto:marcin.szelag@cs.put.poznan.pl">marcin.szelag@cs.put.poznan.pl</a>)
 *
 */
@Data
public class ClassificationTestResult {
	
	/**
	 * Number of correct classification assignments.
	 */
	int numberOfCorrectAssignments;
	
	/**
	 * Number of incorrect classification assignments.
	 */
	int numberOfIncorrectAssignments;
	
	/**
	 * Number of consistent classification assignments.
	 */
	int numberOfConsistentAssignments;
	
	/**
	 * Number of inconsistent classification assignments.
	 */
	int numberOfInconsistentAssignments;
	
	double classificationAccuracy;
	
	double gMean;
	
	double mae;
	
	double rmse;
	
	TruePositiveRateDescription[] truePositiveRates; 
	
}