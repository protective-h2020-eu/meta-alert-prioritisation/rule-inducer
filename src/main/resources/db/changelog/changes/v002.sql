create table metadata (
  id bigserial not null,
  description varchar(255),
  date timestamp without time zone not null,
  name varchar(255),
  data text not null,
  primary key (id)
);