create table models (
  id bigserial not null,
  user_id varchar(255) not null references users(id),
  rules_id bigint not null references rules(id),
  description varchar(255),
  date timestamp without time zone not null,
  name varchar(255),
  primary key(id)
);
