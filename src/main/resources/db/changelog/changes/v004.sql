create table rules (
  id bigserial not null,
  metadata_id bigint not null references metadata (id),
  dataset_id bigint references datasets (id),
  description varchar(255),
  date timestamp without time zone not null,
  name varchar(255),
  data text not null,
  primary key (id)
);