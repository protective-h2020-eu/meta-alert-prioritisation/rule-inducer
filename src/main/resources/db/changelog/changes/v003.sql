create table datasets (
  id bigserial not null,
  metadata_id bigint not null references metadata (id),
  description varchar(255),
  date timestamp without time zone not null,
  name varchar(255),
  data text not null,
  primary key (id)
);